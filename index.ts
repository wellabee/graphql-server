import "reflect-metadata";
import { ApolloServer } from "apollo-server";
//import * as Express from "express";
import { buildSchema } from "type-graphql";

import { EmployeeResolver } from "./resolvers/employeeResolver";

async function main() {
  const schema = await buildSchema({
    resolvers: [EmployeeResolver],
    emitSchemaFile: true,
  });

  //const app = Express();

  const server = new ApolloServer({
    schema,
  });

  //server.applyMiddleware({ app });

  server.listen().then(({ url }) => {
    console.log(`🚀  Server ready at ${url}`);
  });
}

main();
