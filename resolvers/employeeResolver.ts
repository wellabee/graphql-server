import { Query, Resolver, Mutation, Arg } from "type-graphql";
import { Employee } from "../schemas/Employee";

@Resolver((of) => Employee)
export class EmployeeResolver {
  private employees: Employee[] = [
    {
      id: "1",
      name: "Monica Veen",
      profilePicture: "HappyMonicaPicture.png",
      location: { id: "11", name: "Utrecht" },
      joinedDate: new Date("2019-02-12T09:12:32.150Z"),
      role: {
        id: "1",
        title: "Front-end Developer",
      },
      currentMood: {
        title:
          "Bust week but super proud of the results achieved! Feeling Great (4/5)",
        score: 4,
        date: new Date("2021-08-17T09:12:32.150Z"),
      },
      drives: [
        {
          name: "AUTONOMY",
          score: 6.5,
          color: "#18C0FF",
        },
        {
          name: "MASTERY",
          score: 7.1,
          color: "#FDB400",
        },
        {
          name: "PURPOSE",
          score: 9.0,
          color: "#F72B60",
        },
      ],
      courses: [
        {
          id: "1",
          title: "CSS - Basics to Advanced for Front-end development",
        },
        {
          id: "2",
          title: "Tactics for smaller team communication",
        },
      ],
      projects: [
        {
          id: "1",
          title: "Teams in Space",
        },
        {
          id: "2",
          title: "Around the Jupiter",
        },
      ],
      meetings: [
        {
          title: "Daily Standup",
          time: new Date("2021-08-17T04:12:32.150Z"),
          durationInMinutes: 30,
        },
        {
          title: "Project Branstorm",
          time: new Date("2021-08-17T11:30:32.150Z"),
          durationInMinutes: 120,
        },
      ],
    },
    {
      id: "2",
      name: "Sam Fernando",
      profilePicture: "HappySamPicture.png",
      location: { id: "11", name: "Singapore" },
      joinedDate: new Date("2020-03-21T09:12:32.150Z"),
      role: {
        id: "1",
        title: "Back-end Developer",
      },
      currentMood: {
        title: "Wonderful week with awesome new tech stuff (5/5)",
        score: 5,
        date: new Date("2021-08-17T09:12:32.150Z"),
      },
      drives: [
        {
          name: "AUTONOMY",
          score: 7.8,
          color: "#18C0FF",
        },
        {
          name: "MASTERY",
          score: 8.2,
          color: "#FDB400",
        },
        {
          name: "PURPOSE",
          score: 5.4,
          color: "#F72B60",
        },
      ],
      courses: [
        {
          id: "3",
          title: "AWS associate architect",
        },
        {
          id: "4",
          title: "Advanced golang programming",
        },
      ],
      projects: [
        {
          id: "3",
          title: "Fraud ditection",
        },
        {
          id: "4",
          title: "Event driven designing",
        },
      ],
      meetings: [
        {
          title: "Daily Standup",
          time: new Date("2021-08-17T04:12:32.150Z"),
          durationInMinutes: 20,
        },
        {
          title: "Design POC",
          time: new Date("2021-08-17T06:30:32.150Z"),
          durationInMinutes: 60,
        },
      ],
    },
  ];

  @Query((returns) => [Employee], { nullable: true })
  async getEmployees(): Promise<Employee[]> {
    return await this.employees;
  }

  @Query((returns) => Employee, { nullable: true })
  async getEmployee(@Arg("id") id: string): Promise<Employee> {
    const filteredEmployees = await this.employees.filter((emp) => {
      return emp.id === id;
    });

    return filteredEmployees[0];
  }
}
