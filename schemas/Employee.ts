import { Field, ObjectType, InputType } from "type-graphql";

@ObjectType()
export class Course {
  @Field()
  id: string;

  @Field()
  title: string;
}

@ObjectType()
export class Project {
  @Field()
  id: string;

  @Field()
  title: string;
}

@ObjectType()
export class Drive {
  @Field()
  name: string;

  @Field()
  score: number;

  @Field()
  color: string;
}

@ObjectType()
export class Mood {
  @Field()
  title: string;

  @Field()
  score: number;

  @Field()
  date: Date;
}

@ObjectType()
export class Meeting {
  @Field()
  title: string;

  @Field()
  durationInMinutes: number;

  @Field()
  time: Date;
}

@ObjectType()
export class Location {
  @Field()
  id: string;

  @Field()
  name: string;
}

@ObjectType()
export class Role {
  @Field()
  id: string;

  @Field()
  title: string;
}

@ObjectType()
export class Employee {
  @Field()
  id: string;

  @Field()
  name: string;

  @Field()
  profilePicture: string;

  @Field(() => Location)
  location: Location;

  @Field()
  joinedDate: Date;

  @Field(() => Role)
  role: Role;

  @Field(() => Mood)
  currentMood: Mood;

  @Field(() => [Course])
  courses: Course[];

  @Field(() => [Project])
  projects: Project[];

  @Field(() => [Meeting])
  meetings: Meeting[];

  @Field(() => [Drive])
  drives: Drive[];
}

@InputType()
export class EmployeeInput implements Partial<Employee> {
  @Field()
  id: string;
}
